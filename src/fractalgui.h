#ifndef FRACTALGUI_H
#define FRACTALGUI_H
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "mandelbrot.h"
#include "fractal.h"

void print_usage_exit();
int ispnum(char *s);
#endif
 
